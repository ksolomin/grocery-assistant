"""
Grocery assistant.
Note, it may be only 0-30 apples.
If other quantity is required, the answer is "Столько нет"
"""
import termcolor


def main():
    """Add "яблоко" word."""
    n = int(input("Сколько яблок Вам нужно?\n"))

    apple = "яблок"
    if n in (1, 21):
        apple += "о"
    elif n in (2, 3, 4, 22, 23, 24):
        apple += "а"

    if n > 30 or n < 1:
        print("Столько нет")
    elif n >= 0:
        print("Пожалуйста,", n, apple)


if __name__ == "__main__":
    print(termcolor.colored("Grocery assistant", "green", attrs=["blink"]))
    main()
